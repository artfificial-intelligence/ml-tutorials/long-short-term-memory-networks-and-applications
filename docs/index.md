# 장단기 메모리 네트워크와 어플리케이션 <sup>[1](#footnote_1)</sup>

> <font size="3">시퀀스 데이터 분석을 위해 장기 단기 메모리 네트워크를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./memory-networks.md#intro)
1. [장단기 메모리 네트워크란?](./memory-networks.md#sec_02)
1. [장단기 메모리 네트워크는 어떻게 작동할까?](./memory-networks.md#sec_03)
1. [시퀀스 데이터에 장단기 메모리 네트워크가 유용한 이유](./memory-networks.md#sec_04)
1. [Python에서 장단기 메모리 네트워크를 구현하는 방법](./memory-networks.md#sec_05)
1. [장단기 메모리 네트워크를 훈련하고 평가하는 방법](./memory-networks.md#sec_06)
1. [다양한 응용 프로그램에 대한 장단기 메모리 네트워크 사용 방법](./memory-networks.md#sec_07)
1. [마치며](./memory-networks.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 11 — Long Short-Term Memory Networks and Applications](https://ai.gopubby.com/ml-tutorial-11-long-short-term-memory-networks-and-applications-f1989e49ffee?sk=ac94cd8960d0a1e07459529da2f42357)를 편역하였다.
